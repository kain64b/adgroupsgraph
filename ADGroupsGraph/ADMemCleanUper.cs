﻿using System;
using System.Reflection;

namespace ADGroupsGraph
{
    interface ILoginable
    {
        void SetLogin(string domainName, string user, string password);
    }
    class ADApiDomainWrapper<T> : IDisposable where T:ILoginable
    {
        private bool _disposed;
        private AppDomain _adTaskTemporaryDomain;
        public T ADApi { get; private set; }

        public ADApiDomainWrapper(string domainName, string user, string password)
        {
            _adTaskTemporaryDomain = AppDomain.CreateDomain("ADTaskTemporaryDomain" + Guid.NewGuid());
            var currentAssembly = Assembly.GetExecutingAssembly();
            ADApi = (T)_adTaskTemporaryDomain.CreateInstanceAndUnwrap(currentAssembly.GetName().FullName, typeof(Program).ToString());
            ADApi.SetLogin(domainName, user, password);
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    AppDomain.Unload(_adTaskTemporaryDomain);
                }
                ADApi = default(T);
                _adTaskTemporaryDomain = null;
                _disposed = true;
            }
        }
    }
}
