﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices.AccountManagement;
using QuickGraph;
using QuickGraph.Graphviz;
using System.IO;
using QuickGraph.Graphviz.Dot;
using log4net.Config;

namespace ADGroupsGraph
{
    [Serializable]
    class Program : ILoginable
    {
        public static string DomainName, User, Password;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("ADGroupsGraph");
        static void Main(string[] args)
        {
            BasicConfigurator.Configure();
            if (args.Length < 4)
            {
                log.Error("Usage:ADGroupsGraph.exe DomainName User Password Outputfile\n For example: ADGroupsGraph.exe MyDomain admin admin c:\report.dot");
                Environment.Exit(-1);
            }
            DomainName = args[0];
            User = args[1];
            Password = args[2];
            string outputFile = args[3];
            try
            {
                Program p = new Program();
                Dictionary<string, List<string>> edgesMatrix = p.BuildGraphMatrix(DomainName, User, Password);
                SaveToDotFile(outputFile, edgesMatrix);
            }
            catch (Exception e)
            {
                log.Error(e, e);
                Environment.Exit(-1);
            }
        }

        private static void SaveToDotFile(string outputFile, Dictionary<string, List<string>> edgesMatrix)
        {
            Func<KeyValuePair<string, List<string>>, IEnumerable<SEquatableEdge<string>>> keyValueToOutEdges =
                pair =>
                {
                    return pair.Value.Select(v => new SEquatableEdge<string>(pair.Key, v));
                };

            var graph = edgesMatrix.ToVertexAndEdgeListGraph(keyValueToOutEdges);
            graph.ToString();
            var graphVizAlgo = new GraphvizAlgorithm<string, SEquatableEdge<string>>(graph);
            graphVizAlgo.FormatVertex += graphviz_FormatVertex;
            graphVizAlgo.ImageType = GraphvizImageType.Svg;
            string output = graphVizAlgo.Generate();
            File.WriteAllText(outputFile, output);
        }

        static void graphviz_FormatVertex(object sender, FormatVertexEventArgs<string> e)
        {
            //possible bug in lib
        }
        public void SetLogin(string domainName, string user, string password){
            DomainName=user;
            User=user;
            Password=password;
        }
        private Dictionary<string, List<string>> BuildGraphMatrix(string DomainName, string User, string Password)
        {
            Dictionary<string, List<string>> edgesMatrix = new Dictionary<string, List<string>>();
            using (var context = new PrincipalContext(ContextType.Domain, DomainName, User, Password))
            {
                using (var group = new GroupPrincipal(context))
                {
                    List<string> allRootsGroups = GetAllRootGroups(group);
                    edgesMatrix.Add("DOMAIN_ROOT_GROUPS__",allRootsGroups);
                    FillGraphMatrix(edgesMatrix, context, allRootsGroups);
                }
            }
            return edgesMatrix;
        }

        private void FillGraphMatrix(Dictionary<string, List<string>> edgesMatrix, PrincipalContext context, List<string> allRootsGroups)
        {
            foreach (var groupName in allRootsGroups)
            {

                using (var groupPrincipal = GroupPrincipal.FindByIdentity(context, IdentityType.SamAccountName,
                                                                      groupName))
                {
                    List<string> subgroups;
                    using (var wrapper = new ADApiDomainWrapper<Program>(DomainName, User, Password))
                    {
                        subgroups = wrapper.ADApi.GetSubGroupsByGetMembers(groupPrincipal);
                    }
                    
                    if (!edgesMatrix.ContainsKey(groupName))
                    {
                        edgesMatrix.Add(groupName, subgroups);
                        FillGraphMatrix(edgesMatrix, context, subgroups);

                    }
                }
            }
        }
        private List<string> GetSubGroupsByGetMembers(GroupPrincipal group)
        {
            List<string> groups = new List<string>();
            if (group == null)
            {
                return groups;
            }
            try
            {
                using (var results = group.GetMembers())
                {
                    foreach (var item in results)
                    {
                        using (item)
                        {
                            if (item is GroupPrincipal)
                            {
                                groups.Add(item.SamAccountName);
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                log.Error(e, e);
            }
            return groups;
        }
        private List<string> GetAllRootGroups(GroupPrincipal group)
        {
            List<string> groups = new List<string>();
            try
            {
                using (var searcher = new PrincipalSearcher(group))
                {
                    using (var results = searcher.FindAll())
                    {
                        foreach (var item in results)
                        {
                            if (item != null)
                            {
                                using (item)
                                {
                                    if (item is GroupPrincipal)
                                    {
                                        groups.Add(item.SamAccountName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                log.Error(e, e);
            }
            return groups;
        }
    }
}
